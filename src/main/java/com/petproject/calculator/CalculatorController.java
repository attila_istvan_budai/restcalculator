package com.petproject.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/calculator")
public class CalculatorController {
	
	@Autowired
	Evaulator eval;
    
	@ExceptionHandler(value = { IllegalArgumentException.class})
    @RequestMapping(method = RequestMethod.POST)
    Double eval(@RequestBody String input) {
    	return eval.evaulate(input);
    }
		


}
