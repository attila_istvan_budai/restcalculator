package com.petproject.calculator.operation;


public class MinusOperator extends BinaryOperation {
	
	private final String sign = "\\-";
	
	public MinusOperator() {
	}
	

	@Override
	public double calculate(double left, double right) {
		return left-right;
	}
	
	public String getOperatorSign(){
		return sign;
	}

}
