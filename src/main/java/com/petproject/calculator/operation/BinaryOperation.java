package com.petproject.calculator.operation;

import java.util.Arrays;
import java.util.List;

public abstract class BinaryOperation implements Operation{
	
	abstract double calculate(double left, double right);
	
	
	public double calculate(double[] array){
		return Arrays.stream(array).reduce((x,y) -> calculate(x,y)).getAsDouble();
	}
	
	public double calculate(List<Double> array){
		return array.stream().reduce((x,y) -> calculate(x,y)).get();
	}
	
	public String getType(){
		return "Binary";
	}

}
