package com.petproject.calculator.operation;

public class DivideOperator extends BinaryOperation {

	private final String sign = "/";
	
	
	public DivideOperator() {

	}
	

	@Override
	public double calculate(double left, double right) {
		return left/right;
	}

	
	public String getOperatorSign(){
		return sign;
	}

}
