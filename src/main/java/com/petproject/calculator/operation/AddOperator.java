package com.petproject.calculator.operation;


public class AddOperator extends BinaryOperation {
	
	private final String sign = "\\+";
	
	public AddOperator() {
	}

	@Override
	public double calculate(double left, double right) {
		return left + right;
	}

	
	public String getOperatorSign(){
		return sign;
	}


}
