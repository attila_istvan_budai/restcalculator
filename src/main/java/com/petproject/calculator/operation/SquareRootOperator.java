package com.petproject.calculator.operation;

import java.util.List;

public class SquareRootOperator extends UnaryOperation {

	@Override
	public double calculate(List<Double> array) {
		if(array.size()==1){
			return calculate(array.get(0));
		}
		return 0;
	}

	@Override
	public String getOperatorSign() {
		return "sqrt";
	}

	@Override
	public double calculate(double number) {
		return Math.sqrt(number);
	}

}
