package com.petproject.calculator.operation;

import java.util.List;

public interface Operation {
	
	public final String number = "([+-]?([0-9]*[.])?[0-9]+)";
	
	public double calculate(List<Double> array);
	
	abstract String getOperatorSign();
	
	abstract String getType();

}
