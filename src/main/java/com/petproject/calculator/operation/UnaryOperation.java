package com.petproject.calculator.operation;

public abstract class UnaryOperation implements Operation{

	abstract double calculate(double number);
	
	public String getType(){
		return "Unary";
	}
}
