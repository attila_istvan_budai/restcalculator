package com.petproject.calculator.operation;


public class MultiplyOperator extends BinaryOperation {

	private final String sign = "\\*";
	
	public MultiplyOperator() {
	}
	

	@Override
	public double calculate(double left, double right) {
		return left*right;
	}
	
	public String getOperatorSign(){
		return sign;
	}

}
