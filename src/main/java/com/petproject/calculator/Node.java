package com.petproject.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.petproject.calculator.operation.Operation;

public class Node {
	
	private List<Node> children;
	private String data;
	private Operation operation;
	
	public Node(String data) {
		this.children = new ArrayList<Node>();
		this.data = data;
	}
	
	public List<Node> addChildren(Operation operation, String...parts){
		this.operation = operation;
		List<Node> nodes = Arrays.stream(parts).map(x -> new Node(x)).collect(Collectors.toList());
		children.addAll(nodes);
		return nodes;
	}
	

	public boolean isLeaf(){
		return children.size()==0;
	}
	
	public String getData() {
		return data;
	}

	
	public Double evaulate(){
		if(isLeaf()){
			return Double.parseDouble(data);
		} else{
			List<Double> parameters = children.stream().map(x -> x.evaulate()).collect(Collectors.toList());
			return operation.calculate(parameters);
		}
		
	}

	@Override
	public String toString() {
		return "Node [data=" + data + "]";
	}
	
	
	
	

}
