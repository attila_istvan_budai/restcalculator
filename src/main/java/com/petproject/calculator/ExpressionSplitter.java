package com.petproject.calculator;

import java.util.Arrays;

public class ExpressionSplitter {
	
	
	public static String[] split(String expression, String sign){
		return Arrays.stream(expression.split(sign)).filter(x -> !x.isEmpty()).toArray(String[]::new);
	}
	

}
