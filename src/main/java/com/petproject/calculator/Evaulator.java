package com.petproject.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.petproject.calculator.operation.Operation;

public class Evaulator {
	
	List<Operation> operations;
	
	public Evaulator(List<Operation> operations){
		this.operations = operations;
	}

	private String removeWhiteSpaces(String str){
		return str.replaceAll("\\s+","");
	}
	
	public double evaulate(String expression){
		
		if(expression==null || expression.isEmpty()){
			throw new IllegalArgumentException("Cannot process emtpy string");
		}
		
		expression = removeWhiteSpaces(expression);
		

		//operations.add(new SquareRootOperator())
		
		
		
		Node node = new Node(expression);
		List<Node> nodes = new ArrayList<Node>();
		nodes.add(node);
		for(Operation op : operations){
			nodes = nodes.stream()
					.map(x -> x.addChildren(op, ExpressionSplitter.split(x.getData(), op.getOperatorSign())))
					.flatMap(x -> x.stream())
					.collect(Collectors.toList());
		}
		return node.evaulate();
	}

	

	
	

}
