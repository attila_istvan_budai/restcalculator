package com.petproject.calculator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.petproject.calculator.operation.AddOperator;
import com.petproject.calculator.operation.DivideOperator;
import com.petproject.calculator.operation.MinusOperator;
import com.petproject.calculator.operation.MultiplyOperator;
import com.petproject.calculator.operation.Operation;


@Configuration
@ComponentScan(basePackages = {"com.petproject.calculator"})
public class Config {
	
	@Bean
	public Evaulator evaulator(){
		
		List<Operation> operations = new ArrayList<Operation>();
		
		operations.add(new AddOperator());
		operations.add(new MinusOperator());
		operations.add(new MultiplyOperator());
		operations.add(new DivideOperator());
		
		return new Evaulator(operations);
		
	}

}
