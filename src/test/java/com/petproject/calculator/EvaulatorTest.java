package com.petproject.calculator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.petproject.calculator.operation.AddOperator;
import com.petproject.calculator.operation.DivideOperator;
import com.petproject.calculator.operation.MinusOperator;
import com.petproject.calculator.operation.MultiplyOperator;
import com.petproject.calculator.operation.Operation;

import static org.junit.Assert.assertEquals;

public class EvaulatorTest {
	
	Evaulator evaulator;
	
	@Before
	public void setup(){
		List<Operation> operations = new ArrayList<Operation>();
		
		operations.add(new AddOperator());
		operations.add(new MinusOperator());
		operations.add(new MultiplyOperator());
		operations.add(new DivideOperator());
		
		evaulator = new Evaulator(operations);
	}
	
	@Test
	public void testMultiply(){
		Double result = evaulator.evaulate("4 *  4");
		assertEquals(16, result, 0.01);
	}
	
	@Test
	public void testDivide(){
		Double result = evaulator.evaulate("4 /  4");
		assertEquals(1, result, 0.01);
	}
	
	@Test
	public void testAdding(){
		Double result = evaulator.evaulate("4 +  4");
		assertEquals(8, result, 0.01);
	}
	
	@Test
	public void testMinus(){
		Double result = evaulator.evaulate("4 -  4");
		assertEquals(0, result, 0.01);
	}
	
	
	@Test
	public void testNoOperation(){
		Double result = evaulator.evaulate("1");
		assertEquals(1, result, 0.01);
	}

}
