package com.petproject.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.petproject.calculator.operation.AddOperator;
import com.petproject.calculator.operation.DivideOperator;
import com.petproject.calculator.operation.MinusOperator;
import com.petproject.calculator.operation.MultiplyOperator;

public class OperatorTest {
	
	@Test
	public void divideTest(){
		DivideOperator divOp = new DivideOperator();
		
		assertEquals(0.5, divOp.calculate(new double[]{2, 2,2}), 0.01);

	}
	
	@Test
	public void multiplyTest(){
		
		MultiplyOperator mulOp = new MultiplyOperator();
		
		assertEquals(8, mulOp.calculate(new double[]{2, 2,2}), 0.01);
	}
	
	@Test
	public void addingTest(){
		
		AddOperator addOp = new AddOperator();
		
		assertEquals(6, addOp.calculate(new double[]{2, 2,2}), 0.01);
	}
	
	@Test
	public void minusTest(){
		
		MinusOperator minOp = new MinusOperator();
		
		assertEquals(-2, minOp.calculate(new double[]{2, 2,2}), 0.01);
	}

}
