package com.petproject.calculator;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.mockito.Mock;

import com.petproject.calculator.operation.Operation;

public class NodeTest {
	
	@Mock
	Operation operation;
	

	@Test
	public void isLeafTestWhenTrue(){
		Node node = new Node("test");
		assertTrue(node.isLeaf());
		

		
	}
	
	@Test
	public void isLeafTestWhenFalse(){
		Node node = new Node("test");
		node.addChildren(operation, "test2");
		
		assertFalse(node.isLeaf());
				
	}
	
	
	@Test
	public void evaulateTestIfLeaf(){
		Node node = new Node("2");
		assertEquals(2, node.evaulate(), 0.01);
		
	}

}
