# RestCalculator

Simple calculator with REST API


Build the project with "maven clean install" command.

To start the webserver use the "mvn spring-boot:run" command.



The calculator service can be reached on the url: "http://localhost:8080/calculator" by default. 
The service accepts POST commands, the expression should be in the body as plain text.
The service handles + - / * operations.

